FROM opensuse/tumbleweed as build-cert

WORKDIR /tmp

COPY cert-ans.in .

RUN openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout /tmp/quasselCert.pem -out /tmp/quasselCert.pem < /tmp/cert-ans.in

FROM opensuse/tumbleweed as run-image

LABEL maintainer="Drew Adams <druonysus@opensuse.org>"

ARG QUASSELCORE_CONFIGDIR=/var/lib/quasselcore

WORKDIR $QUASSELCORE_CONFIGDIR

RUN zypper -n in quassel-core

USER quasselcore

ADD --chown=quasselcore:quasselcore quasselcore.env /etc/sysconfig/quasselcore
ADD --chown=quasselcore:quasselcore var-lib-quasselcore/* ${QUASSELCORE_CONFIGDIR}/
ADD --chown=quasselcore:quasselcore entrypoint.bash bin/
COPY --chown=quasselcore:quasselcore --from=build-cert /tmp/quasselCert.pem ${QUASSELCORE_CONFIGDIR}/quasselCert.pem

VOLUME /var/lib/quasselcore

EXPOSE ${QUASSELCORE_PORT}

ENTRYPOINT bin/entrypoint.bash
