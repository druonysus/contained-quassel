#!/bin/bash

source /etc/sysconfig/quasselcore

/usr/bin/quasselcore --listen="${QUASSELCORE_LISTEN}" --port="${QUASSELCORE_PORT}" --configdir="${QUASSELCORE_CONFIGDIR}" --add-user | tee "${QUASSELCORE_LOGFILE}"
