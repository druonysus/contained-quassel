build:
	source ./quasselcore.env ; docker build --build-arg QUASSELCORE_CONFIGDIR -t contained-quassel .
run: build
	source ./quasselcore.env ; docker run --env QUASSELCORE_LISTEN --env QUASSELCORE_CONFIGDIR --env QUASSELCORE_PORT -p $${QUASSELCORE_PORT}:$${QUASSELCORE_PORT} contained-quassel
